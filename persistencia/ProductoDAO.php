<?php
class ProductoDAO{
    private $id;
    private $nombre;
    private $precio;
    private $categoria_idcategoria;    
    
    public function ProductoDAO($id="", $nombre="", $precio="", $categoria=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> categoria_idcategoria = $categoria;
    }
    
    public function crear(){
        return "insert into producto (nombre, precio, categoria_idcategoria)
                values (
                '" . $this -> nombre . "',
                '" . $this -> precio . "',
                '" . $this -> categoria_idcategoria . "'
                )";
    }
    
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        return "select idProducto, nombre, precio, categoria_idcategoria
                from producto " . 
                (($atributo != "" && $direccion != "")?"order by " . $atributo . " " . $direccion:"") . 
                " limit " . (($pag-1)*$filas) . ", " . $filas; 
    }
    
    public function consultarTotalFilas(){
        return "select count(idProducto) 
                from producto";
    }
    
}
