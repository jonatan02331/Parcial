<?php
class categoriaDAO{
    private $idcategoria;
    private $nombre;
    
    public function categoriaDAO($id="", $nombre=""){
        $this -> idcategoria = $id;
        $this -> nombre = $nombre;
    }
    
    public function consultar(){
        return "select nombre
                from categoria
                where idcategoria = " . $this -> idcategoria;
    }
    
    public function consultarTodos(){
        return "select idcategoria, nombre
                from categoria
                order by nombre asc";
    }
    
    
    
}
