<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/productoDAO.php";
class Producto{
    private $idProducto;
    private $nombre;
    private $precio;
    private $categoria_idcategoria;   
    private $conexion;
    private $productoDAO;
    
    /**
     * @return string
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @return string
     */
    public function getCategoria_IdCategoria()
    {
        return $this->categoria_idcategoria;
    }

   
    public function Producto($id="", $nombre="", $precio="", $categoria=""){
        $this -> idProducto = $id;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> categoria_idcategoria = $categoria;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($id, $nombre, $precio, $categoria);
    }
    
    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> crear());
        $this -> conexion -> cerrar();
    }
        
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        $this -> conexion -> abrir(); 
       // echo $this -> productoDAO -> consultarTodos($atributo, $direccion, $filas, $pag);
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos($atributo, $direccion, $filas, $pag));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $categoria_idcategoria= new categoria($resultado[3]);
            $categoria_idcategoria -> consultar();
            array_push($productos, new Producto($resultado[0], $resultado[1], $resultado[2], $categoria_idcategoria));            
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarTotalFilas(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTotalFilas());
        return $this -> conexion -> extraer()[0];
    }
    

}