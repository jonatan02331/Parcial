<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/categoriaDAO.php";
class categoria{
    private $idcategoria;
    private $nombre;
    private $Conexion;
    private $categoriaDAO;
        
    /**
     * @return string
     */
    public function getIdCategoria()
    {
        return $this->idcategoria;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function categoria($id="", $nombre=""){
        $this -> idcategoria= $id;
        $this -> nombre = $nombre;
        $this -> Conexion = new Conexion();
        $this -> categoriaDAO = new categoriaDAO($id, $nombre);
    }
    
    public function consultar(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> categoriaDAO -> consultar());
        $resultado = $this -> Conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> Conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> Conexion -> abrir();
        $this -> Conexion -> ejecutar($this -> categoriaDAO -> consultarTodos());
        $categorias = array();
        while(($resultado = $this -> Conexion -> extraer()) != null){
            array_push($categorias, new categoria($resultado[0], $resultado[1]));            
        }
        $this -> Conexion -> cerrar();
        return $categorias;
    }
    
}
