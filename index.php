<?php
require_once "logica/categoria.php";
require_once "logica/Producto.php";
$pid = "";
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}
?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>
<link rel="icon" type="image/png" href="img/logo.png" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
<title>UD-Sports</title>
</head>
<div>
	<form
	action="index.php"
	method="post">
	
		<button type="submit" class="btn btn-light">Inicio</button>
	</form>
</div>
<div class="row">
	<div class="col-2 text-center"></div>
	<div align="center">
		<h1>UD-sports</h1>
	</div>

</div>
<body background="img/fondo1.jpg">
<?php
if ($pid != "") {
    include $pid;
} else {
    include "presentacion/inicio.php";
}
?>
</body>
<div class="col text-center">
ITI &copy; <?php echo date("Y") ?>
				</div>
</html>