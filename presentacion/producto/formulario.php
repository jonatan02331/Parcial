<?php
if (isset($_POST["crear"])) {
    $producto = new Producto("", $_POST["nombre"], $_POST["precio"], $_POST["categoria_idcategoria"]);
    $producto->crear();
}

?>
<div class="row mt-3">
	<div class="col-sm-0 col-md-3"></div>
	<div class="col-sm-12 col-md-6">
		<div class="card">
			<h5 class="card-header" style="background: seagreen">
				<font color="white">Insertar producto</font>
			</h5>
			<div class="card-body">
			<?php if (isset($_POST["crear"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
					role="alert">
					Datos ingresados correctamente.
					<button type="button" class="btn-close" data-bs-dismiss="alert"
						aria-label="Close"></button>
				</div>
					<?php } ?>
				<form
					action="index.php?pid=<?php echo base64_encode("presentacion/producto/formulario.php") ?>"
					method="post">
					
					<div class="mb-3">
						<label class="form-label">Nombre</label> <input type="text"
							class="form-control" name="nombre" required="required">
					</div>
					<div class="mb-3">
						<label class="form-label">Precio</label> <input type="number"
							class="form-control" name="precio" required="required">
					</div>
					<label class="form-label">Categoria</label> <select
						class="form-select" name="categoria_idcategoria">
					<?php
    $categoria = new categoria();
    $categorias = $categoria->consultarTodos();
    foreach ($categorias as $categoriaActual) {
        echo "<option value='" . $categoriaActual->getIdCategoria() . "'>" . $categoriaActual->getNombre() . "</option>";
    }
    ?>			
			</select>

					<button type="submit" name="crear" class="btn btn-light">Insertar</button>
				</form>
			</div>
		</div>
	</div>
</div>

