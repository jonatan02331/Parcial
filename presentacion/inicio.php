<div class="container">
	<div class="row">
		<div>
			<div class="card border-success mb-3">
				<h5 class="card-header" align="center" style="background: seagreen">
					<font color="white">Bienvenido</font>
				</h5>
				<div class="card-body" align="center">
					<div class="d-grid gap-2 col-6 mx-auto">
						<form
							action="index.php?pid=<?php echo base64_encode("presentacion/producto/ProductosAgregar.php") ?>"
							method="post">
							<button class="btn btn-outline-success" type="submit">Agregar
								producto</button>
						</form>

						<form
						action="index.php?pid=<?php echo base64_encode("presentacion/producto/ConsultarProducto.php") ?>"
						method="post"
						>
							<button class="btn btn-outline-success" type="submit">Consultar
								producto</button>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>